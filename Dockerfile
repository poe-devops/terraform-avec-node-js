FROM node:18

WORKDIR /app

# Copier le package.json et le package-lock.json
COPY package*.json ./

# Installer les dépendances
RUN npm install

# Copier le reste des fichiers de l'application
COPY . .

EXPOSE 3000

CMD [ "node", "app.js" ]