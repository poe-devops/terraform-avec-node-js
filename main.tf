resource "docker_image" "nodejsapp" {
    name = "nodejsapp:latest"
    build {
        path = "./"
    }  
}

resource "docker_container" "nodejsapp" {
    image = docker_image.nodejsapp.latest
    name = var.container_name_nodejsapp
    ports {
        internal = 3000
        external = var.container_port_nodejsapp
    } 
    mounts {
      type = "volume"
      source = "node_modules"
      target = "/app"
    }
    mounts {
      type = "volume"
      source = "app_data"
      target = "/app/data"
    }
    depends_on = [docker_container.mongodbserver]
}

