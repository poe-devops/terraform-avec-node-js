variable "container_name_nodejsapp" {
  default     = "my_nodejsapp"
}

variable "container_port_nodejsapp" {
  default     = 3000
}